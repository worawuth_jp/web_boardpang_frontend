export const HEADER_TEXT_1 = 'เริ่มต้นเพียง ';
export const HEADER_TEXT_2 = ' บาท/เดือน';
export const HEADER_TEXT_PRICE = '750';

export const LIST_TEXT = [
  {
    key: 'direct_key_1',
    text: 'บรอดแคสต์ Facebook Fanpage',
  },
  {
    key: 'direct_key_2',
    text: 'บรอดแคสต์ Line Official Account',
  },
  {
    key: 'direct_key_3',
    text: 'ไม่จำกัดเพจ / ไม่จำกัด Line OA',
  },
  {
    key: 'direct_key_4',
    text: 'ใช้งานง่าย ราคาถูก',
  },
  {
    key: 'direct_key_5',
    text: 'มีแอดมินดูแลหลังการขาย',
  },
];
