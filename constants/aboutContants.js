export const HEADER_TEXT_1 = 'ระบบบรอดแคสต์เพิ่มยอดขาย สำหรับแม่ค้าออนไลน์';
export const HEADER_TEXT_2 = 'ผู้ใช้งาน Broadpang';
export const HEADER_TEXT_3 = 'ว่าใช้งานแล้วเป็นยังไงบ้าง ?';
export const LIST_TEXT = [
  {
    key: 'key_1',
    text: 'บรอดแคสต์กลับไปหาลูกค้าเก่า',
  },
  {
      key: 'key_2',
      text: 'จัดโปรโมชั่น กระตุ้นยอดขาย'
  },
  {
      key: 'key_3',
      text: 'ไม่จำกัดจำนวนข้อความ'
  },
  {
      key: 'key_4',
      text: 'ไม่จำกัดเพจ ไม่จำกัด Line OA'
  },
  {
      key: 'key_5',
      text: 'ใช้งานง่าย ราคาถูก'
  },
  {
      key: 'key_6',
      text: 'มีแอดมินดูแลหลังการขาย'
  }
];
