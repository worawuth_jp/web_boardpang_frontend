export const HEADER_TEXT_1 = 'ระบบบอรดแคสต์เพิ่มยอดขาย สำหรับแม่ค้าออนไลน์';
export const HEADER_DESCRIPTION = '(จัดโปรโมชั่นแล้วบรอดแคสต์กลับไปหาลูกค้าที่เคยทักข้อความมาใน Facebook Fanpage และ Line OA เพิ่มยอดขายได้แน่นอน)';
export const LIST_DATA_FUNCTION = [
  {
    key: 'key_1',
    header: 'ระบบบรอดแคสต์ Facebook Fanpage และ Line OA',
    content: 'จัดโปรโมชั่นบรอดแคสต์ไปหาลูกค้าเก่า สื่อสารกลับไป ไม่ให้ลูกค้าลือเรา เพิ่มยอดขายได้ง่าย',
    src: '/images/logo/logo1.jpg',
    padding: false,
  },
  {
    key: 'key_2',
    header: 'บรอดแคสต์ได้แบบไม่จำกัด ',
    content: 'ไม่จำกัดจำนวนข้อความที่บรอดแคสต์ ไม่จำกัดจำนวน Facebook Fanpage ไม่จำกัดจำนวน Line OA',
    src: '/images/icon/envelope.png',
    padding: true,
  },
  {
    key: 'key_3',
    header: 'แอดมินซัพพอร์ต\n',
    content: 'มีแอดมินซัพพอร์ต ดูแลหลังการขาย ติดขัดหรือเกิดปัญหาตรงไหน แจ้งแอดมินในไลน์ซัพพอร์ตได้เลย',
    src: '/images/icon/speak.png',
    padding: true,
  },
  {
    key: 'key_4',
    header: 'ฟังก์ชั่นพิเศษ (ดึงลูกเพจ)\n',
    content: 'ฟังก์ชั่นที่จะช่วยประหยัดเวลาในการดึงลูกเพจเพื่อทำการบรอดแคสต์ และยังสะดวกสบายยิ่งขึ้นในการแบ่งลูกค้าเพื่อบรอดแคสต์ได้',
    src: '/images/icon/community.png',
    padding:false,
  },
  {
    key: 'key_5',
    header: 'เทคนิคบอร์ดแคสต์เพิ่มยอดขาย',
    content: 'มีคลิปวิดิโอเทคนิคการบอร์ดแคสต์แนะนำเรื่องต่างๆ เช่น จัดโปรโมชั่น Headline และไอเดียต่างๆสำหรับการบอร์ดแคสต์',
    src: '/images/icon/increase.png',
    padding: false,
  },
  {
    key: 'key_6',
    header: 'คลังความรู้สำหรับแม่ค้าออนไลน์',
    content: 'คอร์สเรียนเกี่ยวกับการตลาดออนไลน์ในหลายแพลตฟอร์ม สามารถเข้าไปเรียนรู้เพิ่มสกิลให้กับตัวเองได้เลย',
    src: '/images/icon/knowledge.png',
    padding: false,
  },
];
