export const CATEGORIES_ITEM = [
  { key: 'category_1', name: 'ทั้งหมด', value: 'cate-1' },
  { key: 'category_2', name: 'Facebook Marketing', value: 'cate-2' },
  { key: 'category_3', name: 'Logistics', value: 'cate-3' },
  { key: 'category_4', name: 'Marketing', value: 'cate-4' },
  { key: 'category_5', name: 'Online Business', value: 'cate-5' },
  { key: 'category_6', name: 'Packaging', value: 'cate-6' },
  { key: 'category_7', name: 'Payments', value: 'cate-7' },
  { key: 'category_8', name: 'Product', value: 'cate-8' },
  { key: 'category_9', name: 'Reviews', value: 'cate-9' },
];

export const LIST_DATA_FUNCTION = [
  {
    key: 'key_1',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_2',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_3',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_4',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_5',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_6',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
];

export const SHOW_ARTICLE_NUM = 6;
