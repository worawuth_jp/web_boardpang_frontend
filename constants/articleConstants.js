export const HEADER_TEXT_1 = 'บทความแนะนำ';
export const HEADER_DESCRIPTION = '(จัดโปรโมชั่นแล้วบรอดแคสต์กลับไปหาลูกค้าที่เคยทักข้อความมาใน Facebook Fanpage และ Line OA เพิ่มยอดขายได้แน่นอน)';
export const LIST_DATA_FUNCTION = [
  {
    key: 'key_1',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_2',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_3',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_4',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_5',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
  {
    key: 'key_6',
    header: 'แนะนำ ระบบการจัดการหลังร้านใช้งานง่ายคุ้มค่ากับการลงทุน',
    category: 'Online Business',
    date: 'โพสต์เมื่อ 26 ธันวาคม 2022',
    timeRead: 'ใช้เวลาอ่านประมาณ 1 นาที',
    src: 'images/BG/business-woman-arms-up-with-laptop-cardboard-box.jpeg',
  },
];
