const path = require('path');

require('dotenv').config({ path: `../.env.${process.env.NODE_ENV}` });

module.exports = {
  reactStrictMode: true,
  env: {
    ENV: process.env.ENV,
    BASE_API_PATH: process.env.BASE_API_PATH,
    API_VERSION: process.env.API_VERSION,
  },
};
