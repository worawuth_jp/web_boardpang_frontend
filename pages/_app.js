import { Fragment } from 'react';
import { store, wrapper } from '../redux/stores';
import GlobalStyle from '../styles/global';
import Head from 'next/head';
import { createTheme, ThemeProvider } from '@mui/material';
import color from '../styles/variables/color';
import useWindowSize from '../modules/windowSize';

function MyApp({ Component, pageProps }) {
    const theme = createTheme({
        status: {
            danger: '#e53e3e',
        },
        palette: {
            primary: {
                main: color.PRIMARY,
                darker: '#053e85',
            },
            neutral: {
                main: '#64748B',
                contrastText: '#fff',
            },
            dark: {
                main: color.GRAY_COLOR_2,
            },
        },
    });
    let size = useWindowSize();
    return (
        <Fragment>
            <GlobalStyle screenWidth={size.width} screenHeight={size.height} />
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no"></meta>
                <meta property="og:title" content="Broadpang"></meta>
                <meta property="og:image" content="images/logo.png"></meta>
                {/* Font */}
                <link rel="stylesheet" type="text/css" href="/font.css"></link>
                <link rel="shortcut icon" href="images/logo1.ico" />
                <title>Broadpang</title>

                {/* Global Site Tag (gtag.js) - Google Analytics */}
                <script async src={`https://www.googletagmanager.com/gtag/js?id=G-CN954YG8JX`} />
                <script
                    dangerouslySetInnerHTML={{
                        __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-CN954YG8JX', {
              page_path: window.location.pathname,
            });
          `,
                    }}
                />

                {/* <!-- Global site tag (gtag.js) - Google Analytics -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=G-CN954YG8JX"></script>
                <script>
                    window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments)}
                    gtag('js', new Date()); gtag('config', 'G-CN954YG8JX');
                </script> */}
            </Head>
            <ThemeProvider theme={theme}>
                <Component {...pageProps} />
            </ThemeProvider>
        </Fragment>
    );
}

export default wrapper.withRedux(MyApp);
