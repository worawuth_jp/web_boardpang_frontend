import _ from 'lodash';
import { useRouter } from 'next/dist/client/router';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DetailContainer from '../../components/containers/detailContainer';
import MainLayout from '../../components/layouts/mainLayout/mainLayout';
import FooterComponent from '../../components/shared/footer';
import DialogLoading from '../../components/shared/loadingDialog';
import * as constants from '../../constants/indexConstant';
import { setLoadingOff, setLoadingOn } from '../../redux/indexRedux/action';
import ArticleScreenStyle from './style';

function ArticleScreen() {
  const router = useRouter();
  const { articleId } = router.query;

  const indexRef = useRef({});
  const aboutRef = useRef({});
  const functionRef = useRef({});
  const customerRef = useRef({});
  const questionRef = useRef({});
  const priceRef = useRef({});
  const articleRef = useRef({});
  const contactRef = useRef({});
  const mainRef = useRef({});
  const initial = _.reduce(constants.NAV_BAR_KEY, (prev, cur) => {
    let result = { ...prev };
    result[cur.key] = 0;
    _.unset(result, 'name');
    _.unset(result, 'key');
    return result;
  });
  const [scrollTo, setScrollTo] = useState(initial);
  const [componentRef, setComponentRef] = useState(initial);
  const [isShowAllArticle, setShowAllArticle] = useState(false);
  const [isAgreement, setAgreement] = useState(false);
  const [isPolicy, setPolicy] = useState(false);
  const [action, setAction] = useState('');
  const isLoading = useSelector((state) => state.index.options.isLoading);
  const alert = useSelector((state) => state.global.options.alert);
  const articleInfo = useSelector((state) => state.index.constants.articleInfo);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isShowAllArticle && !isAgreement && !isPolicy) {
      setScrollTo((prev) => ({
        ...prev,
        index: indexRef.current.offsetTop,
        about: aboutRef.current.offsetTop,
        function: functionRef.current.offsetTop,
        customerReview: customerRef.current.offsetTop,
        question: questionRef.current.offsetTop,
        price: priceRef.current.offsetTop,
        article: articleRef.current.offsetTop,
        contact: contactRef.current.offsetTop,
      }));

      setComponentRef((prev) => ({
        ...prev,
        index: indexRef.current,
        about: aboutRef.current,
        function: functionRef.current,
        customerReview: customerRef.current,
        question: questionRef.current,
        price: priceRef.current,
        article: articleRef.current,
        contact: contactRef.current,
      }));
    }
  }, [
    indexRef.current.offsetTop,
    aboutRef.current.offsetTop,
    functionRef.current.offsetTop,
    customerRef.current.offsetTop,
    questionRef.current.offsetTop,
    priceRef.current.offsetTop,
    articleRef.current.offsetTop,
    contactRef.current.offsetTop,
    isShowAllArticle,
    isAgreement,
    isPolicy,
  ]); //scrollTo.index, scrollTo.about, scrollTo.function, scrollTo.customerReview

  const closeAlertModal = () => {
    dispatch(closeAlert());
  };

  const routeToLanding = () => {
    setAction('GO_ROOT');
    router.push('/');
  };

  const navHandleClick = (key) => {
    mainRef.current.handdleClickNav(key);
  };

  useEffect(() => {
    dispatch(setLoadingOff());
  }, [articleId]);
  return (
    <ArticleScreenStyle>
      <DialogLoading open={isLoading || action === 'GO_ROOT'} />
      <MainLayout
        navKey="article"
        scrollTo={scrollTo}
        ref={mainRef}
        setAgreement={setAgreement}
        setPolicy={setPolicy}
        componentRef={componentRef}
        setShowArticleDetail={setShowAllArticle}
        isPolicy={isPolicy}
        isAgreement={isAgreement}
        routeToLanding={routeToLanding}
        isShowArticleDetail={isShowAllArticle}>
        <DetailContainer articleId={articleId} />
        <FooterComponent />
      </MainLayout>
    </ArticleScreenStyle>
  );
}

export default ArticleScreen;
