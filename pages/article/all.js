import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import ArticleDetailContainer from '../../components/containers/acticleDetailContainer';
import AllArticleContainer from '../../components/containers/allArticleContainer';
import ArticleContainer from '../../components/containers/articleContainer';
import MainLayout from '../../components/layouts/mainLayout/mainLayout';
import FooterComponent from '../../components/shared/footer';
import DialogLoading from '../../components/shared/loadingDialog';

export default function AllArticlePage() {
  const [navHeight, setNavHeight] = useState(64);
  const isLoading = useSelector((state) => state.index.options.isLoading);

  return (
    <MainLayout navKey="article" setNavHeight={setNavHeight}>
      <DialogLoading open={isLoading} />
      <ArticleDetailContainer />
      <AllArticleContainer />
      <FooterComponent />
    </MainLayout>
  );
}
