import React, { useState } from 'react';
import ArticleContainer from '../../components/containers/articleContainer';
import MainLayout from '../../components/layouts/mainLayout/mainLayout';
import FooterComponent from '../../components/shared/footer';

export default function ArticlePage() {
  const [navHeight, setNavHeight] = useState(64);
  return (
    <MainLayout navKey="article" setNavHeight={setNavHeight}>
      <ArticleContainer navHeight={navHeight} />
      <FooterComponent />
    </MainLayout>
  );
}
