import React, { useEffect, useRef, useState } from 'react';
import IndexContainer from '../components/containers/indexContainer';
import MainLayout from '../components/layouts/mainLayout/mainLayout';
import IndexStyle from './Index/style';
import * as constants from '../constants/indexConstant';
import _ from 'lodash';
import CustomerReview from '../components/containers/customerReviewContainer';
import IndexPage2Container from '../components/containers/indexPage2Container';
import IndexPage3Container from '../components/containers/indexPage3Container';
import AboutContainer from '../components/containers/aboutContainer';
import IndexPage4Container from '../components/containers/indexPage4Container';
import FunctionPageContainer from '../components/containers/functionContainer';
import QuestionContainer from '../components/containers/questionContainer';
import PriceContainer from '../components/containers/priceContainer';
import ArticleContainer from '../components/containers/articleContainer';
import ContactUsContainer from '../components/containers/contactUsContainer';
import FooterComponent from '../components/shared/footer';
import ArticleDetailContainer from '../components/containers/acticleDetailContainer';
import AllArticleContainer from '../components/containers/allArticleContainer';
import classNames from 'classnames';
import AgreementContainer from '../components/containers/agreementContainer';
import PolicyContainer from '../components/containers/policyContainer';
import CoreService from '../services/coreService';
import AlertModal from '../components/shared/AlertModal';
import DialogLoading from '../components/shared/loadingDialog';
import { useDispatch, useSelector } from 'react-redux';
import { closeAlert } from '../redux/globalRedux/action';
import { fetchGetConfigYoutubeAction } from '../redux/indexRedux/action';

export default function Home() {
    const indexRef = useRef({});
    const aboutRef = useRef({});
    const functionRef = useRef({});
    const customerRef = useRef({});
    const questionRef = useRef({});
    const priceRef = useRef({});
    const articleRef = useRef({});
    const contactRef = useRef({});
    const mainRef = useRef({});
    const initial = _.reduce(constants.NAV_BAR_KEY, (prev, cur) => {
        let result = { ...prev };
        result[cur.key] = 0;
        _.unset(result, 'name');
        _.unset(result, 'key');
        return result;
    });
    const [scrollTo, setScrollTo] = useState(initial);
    const [componentRef, setComponentRef] = useState(initial);
    const [isShowAllArticle, setShowAllArticle] = useState(false);
    const [isAgreement, setAgreement] = useState(false);
    const [isPolicy, setPolicy] = useState(false);
    const [navHeight, setNavHeight] = useState(64);
    const isLoading = useSelector((state) => state.index.options.isLoading);
    const alert = useSelector((state) => state.global.options.alert);
    const navKey = useSelector((state) => state.global.constants.navKey);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchGetConfigYoutubeAction());
        if (!isShowAllArticle && !isAgreement && !isPolicy) {
            setScrollTo((prev) => ({
                ...prev,
                index: indexRef.current.offsetTop,
                // about: aboutRef.current.offsetTop + indexRef.current.offsetTop * 3,
                // function: functionRef.current.offsetTop,
                // customerReview: customerRef.current.offsetTop,
                // question: questionRef.current.offsetTop,
                // price: priceRef.current.offsetTop,
                // article: articleRef.current.offsetTop,
                // contact: contactRef.current.offsetTop,
            }));

            setComponentRef((prev) => ({
                ...prev,
                index: indexRef.current,
                // about: aboutRef.current,
                // function: functionRef.current,
                // customerReview: customerRef.current,
                // question: questionRef.current,
                // price: priceRef.current,
                // article: articleRef.current,
                // contact: contactRef.current,
            }));
        }
    }, [
        indexRef.current.offsetTop,
        // aboutRef.current.offsetTop,
        // functionRef.current.offsetTop,
        // customerRef.current.offsetTop,
        // questionRef.current.offsetTop,
        // priceRef.current.offsetTop,
        // articleRef.current.offsetTop,
        // contactRef.current.offsetTop,
        // isShowAllArticle,
        // isAgreement,
        // isPolicy,
    ]); //scrollTo.index, scrollTo.about, scrollTo.function, scrollTo.customerReview

    const closeAlertModal = () => {
        dispatch(closeAlert());
    };

    const navHandleClick = (key) => {
        mainRef.current.handdleClickNav(key);
    };

    return (
        <IndexStyle>
            <AlertModal open={alert.open} onClose={() => closeAlertModal()} />
            <DialogLoading open={isLoading} />
            <MainLayout
                navKey="index"
                scrollTo={scrollTo}
                ref={mainRef}
                setAgreement={setAgreement}
                setPolicy={setPolicy}
                componentRef={componentRef}
                setShowArticleDetail={setShowAllArticle}
                isPolicy={isPolicy}
                isAgreement={isAgreement}
                setNavHeight={setNavHeight}
                isShowArticleDetail={isShowAllArticle}>
                <div className={classNames({ 'display-none': isShowAllArticle || isAgreement || isPolicy })}>
                    <IndexContainer handleClick={navHandleClick} ref={indexRef} navHeight={navHeight} />
                    <IndexPage2Container handleClick={() => navHandleClick('about')} navHeight={navHeight} />
                    <IndexPage3Container handleClick={() => navHandleClick('customerReview')} navHeight={navHeight} />
                    <IndexPage4Container handleClick={() => navHandleClick('price')} navHeight={navHeight} setAgreement={setAgreement} setPolicy={setPolicy} />
                    {/* <AboutContainer ref={aboutRef} navHeight={navHeight} />
          <FunctionPageContainer ref={functionRef} navHeight={navHeight} />
          <CustomerReview ref={customerRef} navHeight={navHeight} />
          <QuestionContainer ref={questionRef} navHeight={navHeight} />
          <PriceContainer ref={priceRef} navHeight={navHeight} />
          <ArticleContainer
            navHeight={navHeight}
            ref={articleRef}
            isShowArticleDetail={isShowAllArticle}
            setPolicy={setPolicy}
            setAgreement={setAgreement}
            setShowArticleDetail={setShowAllArticle}
            scroll={mainRef.current.contentRef}
          />
          <ContactUsContainer ref={contactRef} navHeight={navHeight} /> */}
                </div>
                {/* <div className={classNames({ 'display-none': !isShowAllArticle })}>
          <ArticleDetailContainer />
          <AllArticleContainer />
        </div>

        <div className={classNames({ 'display-none': !isAgreement })}>
          <AgreementContainer />
        </div>

        <div className={classNames({ 'display-none': !isPolicy })}>
          <PolicyContainer />
        </div> */}

                <FooterComponent
                // handleClick={navHandleClick}
                // setPolicy={setPolicy}
                // setAgreement={setAgreement}
                // setShowArticleDetail={setShowAllArticle}
                // scroll={mainRef.current.contentRef}
                />
            </MainLayout>
        </IndexStyle>
    );
}
