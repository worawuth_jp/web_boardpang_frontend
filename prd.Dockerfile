FROM node:16.16.0

#install node
RUN node -v
RUN npm install -g npm

ENV NODE_ENV=prd

# Update the image to the latest packages
RUN apt-get update && apt-get upgrade -y
#
# Install NGINX to test.
RUN apt-get install nginx -y

# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build
EXPOSE 80
CMD [ "npm", "run", "start" ]
