import React, { useEffect, useImperativeHandle, useRef, useState } from 'react';
import MainLayoutStyle from './style';
import Navbar from '../../shared/navbar';
import useWindowSize from '../../../modules/windowSize';
import AlertModal from '../../shared/AlertModal';
import { useDispatch, useSelector } from 'react-redux';
import { clearNavKey, closeAlert, setNavKey } from '../../../redux/globalRedux/action';
import DialogLoading from '../../shared/loadingDialog';
import { setLoadingOff, setLoadingOn } from '../../../redux/indexRedux/action';
const MainLayout = React.forwardRef(
  (
    { children, scrollTo, componentRef, setNavHeight, setShowArticleDetail, isShowArticleDetail, isAgreement, setAgreement, isPolicy, setPolicy, routeToLanding, navKey, ...props },
    ref
  ) => {
    const navRef = useRef({});
    const mainRef = useRef({});
    const contentRef = useRef({});
    const [navbarHeight, setNavbarHeight] = useState(64);
    const [activeTab, setActiveTab] = useState('');
    const [clickName, setClickName] = useState('');
    const [mainHeight, setMainHeight] = useState(null);
    const alert = useSelector((state) => state.global.options.alert);
    const isLoading = useSelector((state) => state.global.options.isLoading);
    const indexIsLoading = useSelector((state) => state.index.options.isLoading);
    const dispatch = useDispatch();
    const closeAlertModal = async () => {
      await dispatch(closeAlert());
    };
    const size = useWindowSize();
    // let diff = {
    //   index: 244,
    //   about: 220,
    //   function: 76,
    //   customerReview: 82,
    //   question: 43,
    //   price: 30,
    //   article: 113,
    //   contact: 0,
    // };
    // const size = useWindowSize();
    // let keyList = Object.keys(scrollTo);
    // let keySort = keyList.sort((a, b) => {
    //   if (scrollTo[a] < scrollTo[b]) {
    //     return -1;
    //   }
    //   if (scrollTo[a] > scrollTo[b]) {
    //     return 1;
    //   }
    //   return 0;
    // });

    // const handleClick = (name) => {
    //   dispatch(setLoadingOn());
    //   if (routeToLanding) {
    //     routeToLanding('/');
    //     dispatch(setNavKey(name));
    //   }
    //   setShowArticleDetail(false);
    //   setPolicy(false);
    //   setAgreement(false);
    //   setClickName(name);

    //   const now = contentRef.current.scrollTop;
    //   if (!isShowArticleDetail && !isAgreement && !isPolicy) {
    //     let sumHeight = 0;
    //     let prevKey = 'index';
    //     for (const key of keySort) {
    //       let cHeight = componentRef[key].offsetHeight;
    //       if (key === 'index') {
    //         cHeight += componentRef[key].offsetHeight * 3;
    //       }
    //       sumHeight += cHeight - diff[key];

    //       if (name === key) {
    //         break;
    //       }
    //       prevKey = key;
    //     }

    //     contentRef.current.scrollTo(now, sumHeight - diff[name] - navbarHeight || 0);
    //     setClickName('');
    //     dispatch(setLoadingOff());
    //     // if (name !== '' && name !== 'index') {
    //     //   dispatch(clearNavKey());
    //     // }
    //   }
    // };

    useImperativeHandle(ref, () => ({
      contentRef: contentRef.current,
      //handdleClickNav: handleClick,
    }));

    useEffect(() => {
      setNavbarHeight(navRef.current.offsetHeight);
      if (setNavHeight) {
        setNavHeight(navRef.current.offsetHeight);
      }
      setMainHeight(mainRef.current.clientHeight);
      // if (!isShowArticleDetail && !isAgreement && !isPolicy && clickName !== '') {
      //   //handleClick(clickName);
      // }
      // if (navKey) {
      //   handleClick(navKey);
      // }

      // if (activeTab !== '') {
      //   dispatch(clearNavKey());
      // }
    }, [navRef, mainRef, navRef.current.offsetHeight, mainRef.current.clientHeight, size, contentRef.current.scrollTop, clickName, navKey, activeTab]);
    return (
      <MainLayoutStyle navbarHeight={navbarHeight} mainHeight={mainHeight} ref={ref}>
        {alert.open && (
          <AlertModal open={alert.open} onClose={() => closeAlertModal()} error={!alert.success && alert.open} success={alert.success && alert.open} message={alert.message} />
        )}
        {/* <DialogLoading open={indexIsLoading} /> */}
        <div className="main" ref={mainRef}>
          <Navbar ref={navRef} navKey={navKey} screenWidth={size.width} /*handleClick={handleClick}*/ keyActiveTab={activeTab} />
          <div
            className="content"
            ref={contentRef}
            // onScroll={(e) => {
            //   let top = e.target.scrollTop;
            //   //console.log(keySort);
            //   setActiveTab('');
            //   let sumHeight = 0;
            //   for (const key of keySort) {
            //     let cHeight = componentRef[key].offsetHeight;
            //     if (key === 'index') {
            //       cHeight += componentRef[key].offsetHeight * 3;
            //     }
            //     sumHeight += cHeight - diff[key];
            //     // sumHeight += componentRef[key].offsetHeight - diff[key];
            //     // console.log(top, 'Key : ', key, ' Scroll ', scrollTo[key], ' Height : ', componentRef[key].offsetHeight, 'SUM : ',sumHeight);
            //     if (top <= componentRef['index'].offsetHeight - 244) {
            //       break;
            //     }
            //     if (top <= sumHeight - diff[key] - navbarHeight) {
            //       setActiveTab(key);
            //       // console.log(key);
            //       break;
            //     }
            //   }
            // }}
          >
            {children}
          </div>
        </div>
      </MainLayoutStyle>
    );
  }
);
export default MainLayout;
