import React, { useEffect, useRef } from 'react';
import ArticleDetailContainerStyle from './style';
import { Container, Grid, Typography } from '@mui/material';
import { Image } from 'react-bootstrap';
import * as constants from '../../../constants/articleDetailContants';
import DateRangeIcon from '@mui/icons-material/DateRange';
import TimerIcon from '@mui/icons-material/Timer';
import color from '../../../styles/variables/color';
import { useDispatch, useSelector } from 'react-redux';
import { fetchRecommendData, setLoadingOff, setLoadingOn } from '../../../redux/indexRedux/action';
import _ from 'lodash';
import { useRouter } from 'next/dist/client/router';
import DialogLoading from '../../shared/loadingDialog';

const ArticleDetailContainer = React.forwardRef((props, ref) => {
  const recommendArticleList = useSelector((state) => state.index.constants.recommendArticle);
  const isLoading = useSelector((state) => state.index.options.isLoading);
  const dispatch = useDispatch();
  const router = useRouter();
  const cardRef = useRef({});

  const callGetRecommendArticle = (id = '') => {
    dispatch(setLoadingOn());
    dispatch(fetchRecommendData());
    dispatch(setLoadingOff());
  };

  const gotoArticleDetail = (id) => {
    dispatch(setLoadingOn());
    router.push(`/article/${id}`);
  };

  useEffect(() => {
    callGetRecommendArticle();
  }, [cardRef]);

  return (
    <ArticleDetailContainerStyle className="bg" cardHeight={cardRef.current.offsetHeight}>
      {/* <DialogLoading open={isLoading} /> */}
      <Container maxWidth="lg" className="detail-container">
        <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" />
        <Grid container direction="row" justifyContent="center" justifyItems="center">
          <Grid container item xs={12} sm={12} md={6} lg={6} xl={6} justifyContent="center" justifyItems="center">
            <Grid container direction="row" justifyContent="center" justifyItems="center" className="grid-padding">
              <div className="display-flex flex-end">
                <Image src="/images/logo/customerReviewLogo.jpg" fluid className="icon" />
                <Typography variant="h4" fontWeight={600}>
                  บทความแนะนำ
                </Typography>
              </div>
            </Grid>

            {_.slice(recommendArticleList, 0, 3).map((item, index) => (
              <Grid
                container
                direction="row"
                justifyContent="center"
                justifyItems="center"
                style={{ width: '100%', height: 'max-content' }}
                key={`ITEMS-ARTICLE-${index}`}
                onClick={() => {
                  gotoArticleDetail(item.articleId);
                }}>
                <div className="card">
                  <div className="header">{item.header}</div>
                  <div className="display-flex justify-center">
                    <span className="date">
                      <DateRangeIcon style={{ fontSize: 20, marginRight: 5 }} />
                      {item.date}
                    </span>

                    <span className="date">
                      <TimerIcon style={{ fontSize: 20, marginRight: 5, marginLeft: 5 }} />
                      {item.timeRead}
                    </span>
                  </div>
                </div>
              </Grid>
            ))}
          </Grid>

          <Grid container item xs={12} sm={12} md={6} lg={6} xl={6} alignContent="center">
            {_.get(recommendArticleList, '[3].articleId', false) && (
              <div className="display-flex flex-center div-relation" style={{ justifyContent: 'center' }}>
                <Image
                  fluid
                  className="img-article"
                  onClick={() => {
                    gotoArticleDetail(_.get(recommendArticleList, '[3].articleId'));
                  }}
                  src={_.get(recommendArticleList, '[3].src')}
                />

                <div
                  ref={cardRef}
                  className="card-recommend"
                  onClick={() => {
                    gotoArticleDetail(_.get(recommendArticleList, '[3].articleId'));
                  }}>
                  <div className="header display-flex justify-center align-center">{_.get(recommendArticleList, '[3].header')}</div>
                  <div className="display-flex justify-center align-center">
                    <span className="date">
                      <DateRangeIcon style={{ fontSize: 20, marginRight: 5 }} />
                      {_.get(recommendArticleList, '[3].date')}
                    </span>

                    <span className="date">
                      <TimerIcon style={{ fontSize: 20, marginRight: 5, marginLeft: 5 }} />
                      {_.get(recommendArticleList, '[3].timeRead')}
                    </span>
                  </div>
                </div>
              </div>
            )}
          </Grid>
        </Grid>
      </Container>
    </ArticleDetailContainerStyle>
  );
});

export default ArticleDetailContainer;
