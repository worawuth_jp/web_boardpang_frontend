import styled from 'styled-components';
import color from '../../../styles/variables/color';
import dimension from '../../../styles/variables/dimension';
import font from '../../../styles/variables/font';

const ArticleDetailContainerStyle = styled.div`
  .image-cover-logo {
    width: 400px;
    height: 120px;
    margin-left: auto;
    margin-right: auto;
    object-fit: cover;
  }

  .align-center {
    text-align: center;
    justify-content: center;
    justify-items: center;
    align-items: center;
    align-content: center;
  }

  .img-article {
    width: auto;
    max-width: calc(100%);
    align-items: center;
    display: flex;
    border-radius: 25px;
    cursor: pointer;
  }

  .card-recommend {
    border-radius: 25px;
    border: solid 1px;
    position: absolute;
    top: 83%;
    left: 12.5%;
    width: 75%;
    min-height: 80px;
    margin-top: 10px;
    background: ${color.WHITE_COLOR};
    padding: 15px;
    white-space: normal;
    cursor: pointer;
  }

  .detail-container {
    text-align: center;
    padding-bottom: 80px;
    margin-top: 0px;
  }

  .icon {
    width: 100px;
    height: 100px;
    margin-left: auto;
    margin-right: auto;
    object-fit: contain;
  }

  .card {
    border-radius: 25px;
    border: solid 1px;
    display: block;
    width: 75%;
    min-height: 50px;
    margin-top: 10px;
    background: ${color.WHITE_COLOR};
    padding: 15px;
    white-space: normal;
    cursor: pointer;
  }

  .grid-padding {
    padding: ${dimension.PADDING_SM} ${dimension.PADDING};
  }

  .date {
    display: flex;
    align-items: center;
    font-size: 18px;
    color: ${color.GRAY_COLOR_3};
  }
  .div-relation {
    position: relative;
    padding: 25px 0px;
    width: 100%;
  }

  .header {
    text-align: left;
    padding: 0px 10px;
    font-size: 24px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    text-overflow: ellipsis;
    overflow: hidden;
  }

  //xs mobile
  @media screen and (max-width: 575px) {
    position: relative;
    padding-bottom: ${(props) => props.cardHeight}px;
    .card-recommend {
      border-radius: 25px;
      border: solid 1px;
      position: absolute;
      top: 75%;
      left: 12.5%;
      width: 75%;
      min-height: 80px;
      margin-top: 10px;
      background: ${color.WHITE_COLOR};
      padding: 15px;
      white-space: normal;
      cursor: pointer;
    }

    & .image-cover-logo {
      width: 250px;
      height: 80px;
    }

    & .icon {
      width: 70px;
      height: 70px;
    }

    & .grid-padding {
      h4 {
        font-size: 30px;
      }
    }

    & .header {
      font-size: 26px;
    }

    & .display-flex {
      display: block;
    }

    & .date {
      text-align: left;
      svg {
        margin-left: 0px !important;
      }
    }
  }

  //sm tablet
  @media screen and (min-width: 576px) and (max-width: 767px) {
  }

  //md extra tablet
  @media screen and (min-width: 768px) and (max-width: 991px) {
  }
`;

export default ArticleDetailContainerStyle;
