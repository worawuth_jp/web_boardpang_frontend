import { Button, Container, Grid, Typography } from '@mui/material';
import React, { useEffect, useRef } from 'react';
import { Image } from 'react-bootstrap';
import FunctionContainerStyle from './style';
import * as constants from '../../../constants/functionConstants';
import CustomCard from './CustomCard';

const FunctionPageContainer = React.forwardRef((props, ref) => {
  return (
    <FunctionContainerStyle className="bg" ref={ref} navHeight={props.navHeight}>
      <a target="_blank" href="https://lin.ee/wtEcDjy">
        <Image className="chat-icon" src="images/icon/chat.png" />
      </a>
      {/* <div className="main-content">
        <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" />
        <Typography variant="h5">{constants.HEADER_TEXT_1}</Typography>
        <Typography variant="p" className="text-muted">
          {constants.HEADER_DESCRIPTION}
        </Typography>

        <Grid container direction="row" columnSpacing={2} alignContent="center" alignItems="center">
          {constants.LIST_DATA_FUNCTION.map((item) => (
            <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} key={`LIST_FUNCTION_${item.key}`}>
              <CustomCard title={item.header} src={item.src}>
                {item.content}
              </CustomCard>
            </Grid>
          ))}
        </Grid>
      </div> */}
      <div className="app-container">
        <Container maxWidth="xxl" className="content-container">
          <div className="main-content">
            <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" />
            <Typography variant="h5" style={{ fontWeight: 600, marginBottom: 10, fontSize: '1.4rem' }}>
              {constants.HEADER_TEXT_1}
            </Typography>
            <Typography variant="p" className="text-muted">
              {constants.HEADER_DESCRIPTION}
            </Typography>

            <Grid
              container
              direction="row"
              columnSpacing={{ xs: 2, lg: 8 }}
              rowSpacing={{ xs: 2, lg: 2 }}
              alignContent="center"
              alignItems="start"
              sx={{ marginTop: { lg: '2%' }, marginBottom: { lg: '2%' } }}>
              {/* {constants.LIST_DATA_FUNCTION.map((item) => (
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} key={`LIST_FUNCTION_${item.key}`} alignContent="center" justifyContent="center">
                <CustomCard title={item.header} src={item.src} pad={item.padding}>
                  {item.content}
                </CustomCard>
              </Grid>
            ))} */}
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard
                  title={
                    <div>
                      ระบบบรอดแคสต์
                      <br />
                      Facebook Fanpage และ Line OA
                    </div>
                  }
                  src="/images/logo/logo1.jpg"
                  srcClass>
                  จัดโปรโมชั่นบรอดแคสต์ไปหาลูกค้าเก่า
                  <br />
                  สื่อสารกลับไป ไม่ให้ลูกค้าลืมเรา
                  <br />
                  เพิ่มยอดขายได้ง่ายๆ
                </CustomCard>
              </Grid>
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard title="บรอดแคสต์ได้แบบไม่จำกัด" src="/images/icon/envelope.png">
                  ไม่จำกัดจำนวนข้อความที่บรอดแคสต์
                  <br />
                  ไม่จำกัดจำนวน Facebook Fanpage
                  <br />
                  ไม่จำกัดจำนวน Line OA
                </CustomCard>
              </Grid>
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard title="แอดมินซัพพอร์ต" src="/images/icon/speak.png">
                  มีแอดมินซัพพอร์ต ดูแลหลังการขาย
                  <br />
                  ติดขัดหรือเกิดปัญหาตรงไหน
                  <br />
                  แจ้งแอดมินในไลน์ซัพพอร์ตได้เลย
                </CustomCard>
              </Grid>
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard title="ฟังก์ชั่นพิเศษ (ดึงลูกเพจ)" src="/images/icon/community.png">
                  ฟังก์ชั่นที่จะช่วยประหยัดเวลาในการดึงลูกเพจ
                  <br />
                  เพื่อทำการบรอดแคสต์ และยังสะดวกสบาย
                  <br />
                  ยิ่งขึ้นในการแบ่งลูกค้าเพื่อบรอดแคสต์ได้
                </CustomCard>
              </Grid>
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard title="เทคนิคบอร์ดแคสต์เพิ่มยอดขาย" src="/images/icon/increase.png">
                  มีคลิปวิดิโอเทคนิคการบอร์ดแคสต์
                  <br />
                  แนะนำเรื่องต่างๆ เช่น จัดโปรโมชั่น Headline
                  <br />
                  และไอเดียต่างๆสำหรับการบอร์ดแคสต์
                </CustomCard>
              </Grid>
              <Grid item container direction="column" xs={12} sm={12} md={6} lg={4} xl={4} alignContent="center" justifyContent="center">
                <CustomCard
                  title={
                    <div>
                      คลังความรู้
                      <br />
                      สำหรับแม่ค้าออนไลน์
                    </div>
                  }
                  src="/images/icon/knowledge.png">
                  คอร์สเรียนเกี่ยวกับการตลาดออนไลน์
                  <br />
                  ในหลายแพลตฟอร์ม สามารถเข้าไปเรียนรู้
                  <br />
                  เพิ่มสกิลให้กับตัวเองได้เลย
                </CustomCard>
              </Grid>
            </Grid>
          </div>
        </Container>
      </div>
    </FunctionContainerStyle>
  );
});

export default FunctionPageContainer;
