import { SettingsPower } from '@mui/icons-material';
import { Button, Container, Grid, Typography } from '@mui/material';
import Link from 'next/link';
import React, { useEffect, useRef, useState } from 'react';
import { Image } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import * as constants from '../../../constants/articleConstants';
import { setLoadingOff, setLoadingOn } from '../../../redux/globalRedux/action';
import { getArticleList } from '../../../redux/indexRedux/action';
import ArticleNotFound from '../../shared/articleNotFound';
import DialogLoading from '../../shared/loadingDialog';
import CustomCard from './CustomCard';
import ArticleContainerStyle from './style';

const ArticleContainer = React.forwardRef((props, ref) => {
  const dispatch = useDispatch();
  const articleLists = useSelector((state) => {
    return state.index.constants.articleList;
  });
  const isLoading = useSelector((state) => state.index.options.isLoading);
  const setShowArticleDetail = (status) => {
    props.setShowArticleDetail(status);
  };

  const callArticle6BlockApi = async () => {
    await dispatch(getArticleList());
  };

  useEffect(() => {
    callArticle6BlockApi();
  }, []);
  return (
    <ArticleContainerStyle className="bg" ref={ref}>
      <DialogLoading open={isLoading} />
      <Container maxWidth="lg" className="content-container">
        <div className="main-content">
          <Typography variant="h4" display="flex" justifyItems="center" justifyContent="center" alignItems="center">
            <Image src="/images/logo/16.png" fluid className="header-icon" />
            <div style={{ textAlign: 'center', width: 'auto' }}>{constants.HEADER_TEXT_1}</div>
          </Typography>
          <div style={{ textAlign: 'end' }}>
            <Link href="/article/all">
              <Button
                variant="contained"
                size="small"
                className="btn-button"
                style={{ width: 'auto', padding: '0px 5%' }}
                // onClick={(e) => {
                //   // props.scroll.scrollTo(0, 0);
                //   // props.setAgreement(false);
                //   // props.setPolicy(false);
                //   // setShowArticleDetail(true);
                //   e.preventDefault(true);
                // }}
              >
                บทความทั้งหมด
              </Button>
            </Link>
          </div>
          <Grid container direction="row" columnSpacing={4} alignContent="center">
            {articleLists.map((item) => (
              <Grid item container direction="column" xs={12} sm={6} md={6} lg={4} xl={4} key={`LIST_FUNCTION_${item.key}`}>
                <CustomCard title={item.header} src={item.src} tag={item.category} date={item.date} articleId={item.articleId} timeRead={item.timeRead}>
                  {item.content}
                </CustomCard>
              </Grid>
            ))}
          </Grid>
          {articleLists.length === 0 && <ArticleNotFound />}
        </div>
      </Container>
    </ArticleContainerStyle>
  );
});

export default ArticleContainer;
