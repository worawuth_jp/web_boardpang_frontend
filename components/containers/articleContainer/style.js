import styled from 'styled-components';

const ArticleContainerStyle = styled.div`
  //min-height: 80vh !important;
  min-height: calc(100vh - ${(props) => props.navHeight}px) !important;
  padding: 0px 5% 5%;
  .header {
    display: block;
    width: 100%;
  }

  & .btn-button {
    font-size: 30px;
  }

  .main-content {
    h4 {
      font-weight: 600;
    }
  }

  .header-icon {
    width: 90px;
  }

  .img-content {
    width: 35em;
    height: 25em;
    object-fit: cover;
  }

  .image-cover-logo {
    width: 400px;
    height: 100px;
    margin-left: auto;
    margin-right: auto;
    object-fit: cover;
  }

  .list-text {
    display: block !important;

    .list-item {
      margin-left: 80px;
      display: flex;
      align-items: center;
      align-content: center;
    }

    img {
      width: 30px;
      margin-right: 15px;
    }
  }

  //xs mobile
  @media screen and (max-width: 575px) {
    & .btn-button {
      font-size: 20px;
    }
  }

  //sm tablet
  @media screen and (min-width: 576px) and (max-width: 767px) {
  }
`;

export default ArticleContainerStyle;
