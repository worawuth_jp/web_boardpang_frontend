import { Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { Image } from 'react-bootstrap';
import AboutContainerStyle from './style';
import * as constants from '../../../constants/aboutContants';

const AboutContainer = React.forwardRef((props, ref) => {
  return (
    <AboutContainerStyle className="bg" ref={ref} navHeight={props.navHeight}>
      <a target="_blank" href="https://lin.ee/wtEcDjy">
        <Image className="chat-icon" src="images/icon/chat.png" />
      </a>
      <div className="app-container">
        <Grid container direction="row" spacing="2" textAlign="center" className="header-text-page">
          <Grid container item direction="column" alignContent="center" xs={12} sm={12} md={6} lg={6} xl={6}>
            <Image className="img-content" src="images/icon/seller.png" fluid />
          </Grid>
          <Grid container item direction="column" alignContent="center" alignItems="center" className="about-header" xs={12} sm={12} md={6} lg={6} xl={6}>
            <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" />
            <Typography style={{ fontWeight: 600, fontSize: '1.8rem', marginTop: 10 }} variant="h4">
              {constants.HEADER_TEXT_1}
            </Typography>

            <Grid
              container
              direction="row"
              display={'block'}
              spacing="2"
              className="header-text-page"
              style={{ marginTop: { xs: 20, lg: 30, xl: 30 }, width: 'auto', marginBottom: 50 }}>
              {constants.LIST_TEXT.map((item) => (
                <div className="list-text" key={`Typography_${item.key}`}>
                  <div variant="div" className="list-item">
                    <Image src="images/icon/check-mark.png" />
                    {item.text}
                  </div>
                </div>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </div>
      {/* <Container maxWidth="lg" className="content-container">
        <Grid container direction="row" spacing="2" textAlign="center" className="header-text-page">
          <Grid container item direction="column" alignContent="center" alignItems="center" xs={12} sm={12} md={6} lg={6} xl={6}>
            <Image className="img-content" src="images/icon/seller.png" fluid />
          </Grid>
          <Grid container item direction="column" alignContent="center" alignItems="center" className="about-header" xs={12} sm={12} md={6} lg={6} xl={6}>
            <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" />
            <Typography variant="h4">{constants.HEADER_TEXT_1}</Typography>

            <Grid
              container
              direction="row"
              display={'block'}
              spacing="2"
              className="header-text-page"
              style={{ marginTop: { xs: 20, lg: 30, xl: 30 }, width: 'auto', marginBottom: 50 }}>
              {constants.LIST_TEXT.map((item) => (
                <div className="list-text" key={`Typography_${item.key}`}>
                  <div variant="div" className="list-item">
                    <Image src="images/icon/check-mark.png" />
                    {item.text}
                  </div>
                </div>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container> */}
    </AboutContainerStyle>
  );
});

export default AboutContainer;
