import { Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { Image } from 'react-bootstrap';
import Link from 'next/link';
import IndexContainerStyle from './style';
import * as constants from '../../../constants/indexConstant';
import useWindowSize from '../../../modules/windowSize';
import { useSelector } from 'react-redux';
const IndexContainer = React.forwardRef((props, ref) => {
    let size = useWindowSize();
    const configYoutube = useSelector((state) => state.index.constants.configYoutube);
    return (
        <IndexContainerStyle className="bg-1" ref={ref} navHeight={props.navHeight} screenHeight={size.height}>
            <a target="_blank" href="https://lin.ee/wtEcDjy">
                <Image className="chat-icon" src="images/icon/chat.png" />
            </a>

            <div className="app-container flex">
                <Grid container direction="row" spacing="2" className="row-content" alignItems="center">
                    <Grid item container xs={12} sm={12} md={6} lg={6} xl={6} alignItems="center" justifyContent="center" alignContent="center">
                        <iframe
                            width="100%"
                            height="100%"
                            className="video-yt"
                            src={configYoutube.block1}
                            allow="accelerometer; autoplay; clipboard-write; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                    </Grid>
                    <Grid
                        item
                        container
                        xs={12}
                        sm={12}
                        md={6}
                        lg={6}
                        xl={6}
                        // style={{
                        //   height: '100%',
                        //   marginBottom: 'auto',
                        //   marginTop: 'auto',
                        //   paddingLeft: 50,
                        // }}
                    >
                        <Image component="div" src="/images/logo.png" fluid className="image-cover-logo" style={{ marginTop: 'auto' }} />
                        <div
                            className="header text-center"
                            style={{
                                marginBottom: 'auto',
                            }}>
                            <div className="index-text">
                                ระบบบรอดแคสต์เพิ่มยอดขาย <br /> สำหรับแม่ค้าออนไลน์
                            </div>

                            {/* <div className="index-text"></div> */}
                            <a target="_blank" href="https://lin.ee/wtEcDjy">
                                <Button variant="contained" size="small" className="btn-button btn-index" style={{ marginTop: 50 }}>
                                    เริ่มต้นใช้งาน
                                </Button>
                            </a>
                        </div>
                    </Grid>
                </Grid>
            </div>
            {/* <IndexPage2Container handleClick={() => props.handleClick('about')} />
      <IndexPage3Container handleClick={() => props.handleClick('customerReview')} />
      <IndexPage4Container handleClick={() => props.handleClick('price')} /> */}
        </IndexContainerStyle>
    );
});
export default IndexContainer;
