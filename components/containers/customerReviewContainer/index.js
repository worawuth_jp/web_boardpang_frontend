import { Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { Image } from 'react-bootstrap';
import CustomerReviewStyle from './style';
import * as constants from '../../../constants/customerReviewConstant';
import ReviewCarousel from './ReviewCarousel';
import VideoCarousel from './videoCarousel';

const CustomerReview = React.forwardRef((props, ref) => {
    return (
        <CustomerReviewStyle className="bg" ref={ref} navHeight={props.navHeight}>
            <a target="_blank" href="https://lin.ee/wtEcDjy">
                <Image className="chat-icon" src="images/icon/chat.png" />
            </a>
            <div className="app-container">
                <Grid container direction="row" spacing="2" textAlign="center" display="block" className="header-text-page">
                    <Typography variant="h4" fontWeight={600} fontSize={'1.7rem'} display="flex" justifyItems="center" justifyContent="center" alignItems="center">
                        <Image src="/images/logo/customerReviewLogo.jpg" fluid className="header-icon" />
                        {constants.HEADER_PAGE_TEXT}
                    </Typography>
                </Grid>

                <Grid container direction="row" spacing="2" textAlign="center" display="block">
                    <Typography className="description" variant="span">
                        {constants.CONTENT_HEADER_TEXT}
                    </Typography>
                </Grid>
                <div className="review-wrapper">
                    <Grid container direction="row" display="block" spacing="2" alignContent="center" alignItems="center">
                        <VideoCarousel />
                        <ReviewCarousel />
                    </Grid>
                </div>
            </div>
            {/* <Container className="customer-container" maxWidth="lg">
        <Grid container direction="row" display="block" spacing="2" alignContent="center" alignItems="center">
          <ReviewCarousel />
        </Grid>
      </Container> */}
        </CustomerReviewStyle>
    );
});

export default CustomerReview;
