import { Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import * as constants from '../../../constants/indexPage3Contants';
import { Image } from 'react-bootstrap';
import IndexPage3ContainerStyle from './style';
import useWindowSize from '../../../modules/windowSize';
import windowSize from '../../../styles/variables/windowSize';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';

const IndexPage3Container = React.forwardRef((props, ref) => {
    let size = useWindowSize();
    const configYoutube = useSelector((state) => state.index.constants.configYoutube);
    const router = useRouter();
    return (
        <IndexPage3ContainerStyle className="bg-1" navHeight={props.navHeight} screenWidth={size.width} screenHeight={size.height}>
            <Image className="vector" src="/images/icon/Vector2.png" />
            <a target="_blank" href="https://lin.ee/wtEcDjy">
                <Image className="chat-icon" src="images/icon/chat.png" />
            </a>
            <div className="app-container">
                <div className="index-wrapper">
                    <Grid container direction="row" spacing="2" columnSpacing={3} justifyContent="center" justifyItems="center">
                        <Grid item container xs={12} sm={12} md={8} lg={6} xl={6}>
                            <div className="header">
                                <div className="text-header-1">
                                    {constants.HEADER_TEXT_1}
                                    <Image src="/images/logo/customerReviewLogo.jpg" fluid className="header-icon" />
                                </div>
                                <Typography variant="h3">{constants.HEADER_TEXT_2}</Typography>
                                <div className="text-header-1" variant="h5" style={{ whiteSpace: 'normal' }}>
                                    {constants.HEADER_TEXT_3}
                                </div>
                            </div>

                            <div className="card-review ">
                                "จากที่เงียบเหงา มานานเพราะ โดนปิดกั้นการมองเห็น
                                <br /> พอได้ใช้ระบบ ลูกค้าเก่าเริ่มทยอยกับมาซื้อแถมยัง
                                <br />
                                ได้ลูกค้าใหม่ๆเพิ่มขึ้นมาอีกเพียบ
                                <br />
                                บรอดแคสต์ทีนึง ก็ได้ค่าระบบคืนแล้วแถมกำไรยังตามมาอีกเรื่อยๆเลยจ้า
                                <br /> แอดมินน่ารักมากกกกก ถามเยอะแค่ไหน ก็ไม่บ่น
                                <br /> ไม่เข้าใจตรงไหน ก็ช่วยทำให้ บริการก่อน-หลังการขาย คือดีงานมมากค่ะ"
                            </div>

                            <Grid container direction="row" style={{ marginTop: 20 }} alignItems="center" alignContent="center">
                                <div className="display-flex profile-wrapper">
                                    <div className="profile-icon"></div>
                                    <div className="profile-text">
                                        <span>{constants.PROFILE_INFO_TEXT}</span>
                                        <br />
                                        {constants.PROFILE_INFO_CONTENT}
                                    </div>
                                </div>
                                {/* <Grid xs={12} item container direction="column" style={{ width: 'auto' }}>
                <div className="profile-icon"></div>
              </Grid>
              <Grid xs={12} item container direction="column" style={{ width: 'auto', paddingLeft: 10 }}>
                {constants.PROFILE_INFO_TEXT}
                <br />
                {constants.PROFILE_INFO_CONTENT}
              </Grid> */}
                            </Grid>
                        </Grid>

                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={4}
                            lg={6}
                            xl={6}
                            style={{
                                height: '100%',
                                marginBottom: { xl: 'auto' },
                                marginTop: 'auto',
                            }}>
                            <iframe
                                width="100%"
                                height="100%"
                                className="video-yt"
                                src={configYoutube.block3}
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
                        </Grid>
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        spacing="2"
                        display="block"
                        textAlign="center"
                        position="absolute"
                        sx={{ bottom: { lg: '5%', xl: '1%' }, width: size.width >= windowSize.STD_WIDTH ? windowSize.STD_WIDTH : size.width }}>
                        <Link href="/customer-review">
                            <Button
                                variant="contained"
                                size="small"
                                className="btn-button review-btn"
                                onClick={() => {
                                    router.replace('/customer-review');
                                }}>
                                รีวิวเพิ่มเติม
                            </Button>
                        </Link>
                    </Grid>
                </div>
            </div>
            {/* <Container maxWidth="lg" className="content-container">
        <Grid container direction="row" spacing="2" columnSpacing={3} justifyContent="center" justifyItems="center">
          <Grid item container xs={12} sm={12} md={8} lg={8} xl={8}>
            <div className="header text-center">
              <Typography variant="h5">
                {constants.HEADER_TEXT_1}
                <Image src="/images/logo/customerReviewLogo.jpg" fluid className="header-icon" />
              </Typography>
              <Typography variant="h4">{constants.HEADER_TEXT_2}</Typography>
              <Typography variant="h5" style={{ whiteSpace: 'normal' }}>
                {constants.HEADER_TEXT_3}
              </Typography>
            </div>

            <div className="card-review">{constants.CONTENT_TEXT}</div>

            <Grid container direction="row" style={{ marginTop: 20 }} alignItems="center" alignContent="center">
              <div className="display-flex profile-wrapper">
                <div className="profile-icon" ></div>
                <div className="profile-text">
                  <span>{constants.PROFILE_INFO_TEXT}</span>
                  <br />
                  {constants.PROFILE_INFO_CONTENT}
                </div>
              </div>
              {/* <Grid xs={12} item container direction="column" style={{ width: 'auto' }}>
                <div className="profile-icon"></div>
              </Grid>
              <Grid xs={12} item container direction="column" style={{ width: 'auto', paddingLeft: 10 }}>
                {constants.PROFILE_INFO_TEXT}
                <br />
                {constants.PROFILE_INFO_CONTENT}
              </Grid> */}
            {/* </Grid>
          </Grid>

          <Grid
            item
            container
            xs={12}
            sm={12}
            md={4}
            lg={4}
            xl={4}
            style={{
              height: '100%',
              marginBottom: 'auto',
              marginTop: 'auto',
            }}>
            <iframe
              width="100%"
              height="100%"
              className="video-yt"
              src="https://www.youtube.com/embed/8IB9xbP_keI"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
          </Grid>
        </Grid>

        <Grid container direction="row" spacing="2" display="block" textAlign="center">
          <Button variant="contained" size="small" className="btn-button review-btn" onClick={() => props.handleClick()}>
            รีวิวเพิ่มเติม
          </Button>
        </Grid>
      </Container> {' '}
      */}
        </IndexPage3ContainerStyle>
    );
});

export default IndexPage3Container;
