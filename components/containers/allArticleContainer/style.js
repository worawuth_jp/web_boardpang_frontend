import styled from 'styled-components';
import color from '../../../styles/variables/color';
import dimension from '../../../styles/variables/dimension';
import font from '../../../styles/variables/font';

const AllArticleContainerStyle = styled.div`
  position: relative;
  .header-icon {
    width: 85px;
    height: 85px;
  }
  .image-cover-logo {
    width: 200px;
    height: 70px;
    margin-left: auto;
    margin-right: auto;
    object-fit: cover;
  }

  .img-article {
    width: 80%;
    align-items: center;
    display: flex;
    border-radius: 25px;
  }

  .card-recommend {
    border-radius: 25px;
    border: solid 1px;
    position: absolute;
    bottom: 10%;
    left: 7%;
    width: 65%;
    min-height: 65px;
    margin-top: 10px;
    background: ${color.WHITE_COLOR};
    padding: 15px;
    white-space: normal;
  }

  .detail-container {
    text-align: center;
    padding-bottom: 80px;
    margin-top: 25px;
  }

  .icon {
    width: 85px;
    height: 85px;
    margin-left: auto;
    margin-right: auto;
    object-fit: contain;
  }

  .card {
    border-radius: 25px;
    border: solid 1px;
    display: block;
    width: 65%;
    min-height: 65px;
    margin-top: 10px;
    background: ${color.WHITE_COLOR};
    padding: 15px;
    white-space: normal;
  }

  .grid-padding {
    padding: ${dimension.PADDING_SM} ${dimension.PADDING};
  }

  .date {
    display: flex;
    align-items: center;
    font-size: ${font.FONT_SIZE_PX.SM};
    color: ${color.GRAY_COLOR_3};
  }
  .div-relation {
    position: relative;
  }

  .rounded {
    border-top: 8px solid;
    border-radius: 5px;
    border-color: ${color.PRIMARY};
    margin-top: 15px;
  }

  .category {
    display: flex;
    align-items: center;
    align-content: center;
  }

  .category-item {
    display: flex;
    padding: 10px 25px;
    cursor: pointer;
    font-weight: 550;
  }

  .select {
    border-top: 8px solid;
    border-radius: 5px;
    border-color: ${color.WHITE_COLOR};
    margin-top: 15px;
  }
  .category-item:hover {
    border-bottom: 8px solid;
    border-radius: 5px;
    border-color: ${color.PRIMARY};
  }

  .active {
    border-bottom: 8px solid;
    border-radius: 5px;
    border-color: ${color.PRIMARY};
  }

  .category-item:first-child {
    margin-left: auto;
  }
  .category-item:last-child {
    margin-right: auto;
  }

  .mb-5 {
    margin-bottom: 50px;
  }

  & .chat-icon {
    bottom: 1%;
  }

  //xs mobile
  @media screen and (max-width: 575px) {
  }

  //sm tablet
  @media screen and (min-width: 576px) and (max-width: 767px) {
  }
`;

export default AllArticleContainerStyle;
