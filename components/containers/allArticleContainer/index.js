import React, { useEffect, useState } from 'react';
import { Container, Grid, Pagination, PaginationItem, Tab, Tabs, ThemeProvider, Typography } from '@mui/material';
import { Image } from 'react-bootstrap';
import AllArticleContainerStyle from './style';
import * as constants from '../../../constants/allArticleConstants';
import color from '../../../styles/variables/color';
import { createTheme } from '@mui/material/styles';
import CustomCard from './CustomCard';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { fetchingArticlePaginate, fetchingCategoryData, fetchRecommendData, setLoadingOff, setLoadingOn } from '../../../redux/indexRedux/action';
import ArticleNotFound from '../../shared/articleNotFound';
import PaginateComponent from '../../shared/paginateComponent';

const AllArticleContainer = React.forwardRef((props, ref) => {
  const [activeTab, setActiveTab] = useState(0);
  const [page, setPage] = useState(1);
  const categoryList = useSelector((state) => state.index.constants.category);
  const recommendArticleList = useSelector((state) => state.index.constants.recommendArticle);
  const allArticleList = useSelector((state) => state.index.constants.allArticleList);
  const dispatch = useDispatch();
  // const theme = createTheme({
  //   status: {
  //     danger: '#e53e3e',
  //   },
  //   palette: {
  //     primary: {
  //       main: `${color.PRIMARY}`,
  //       darker: '#053e85',
  //     },
  //     neutral: {
  //       main: '#64748B',
  //       contrastText: '#fff',
  //     },
  //   },
  // });

  const handleClick = (name) => {
    setActiveTab(name);
  };

  const callApiGetArticle = (pageNo = 1, pageSize = constants.SHOW_ARTICLE_NUM, categoryId = 0) => {
    setPage(pageNo);
    dispatch(fetchingArticlePaginate(pageNo, pageSize, categoryId));
  };

  const callCategory = (id = '') => {
    dispatch(setLoadingOn());
    dispatch(fetchingCategoryData(id));
    dispatch(setLoadingOff());
  };

  const callGetRecommendArticle = (id = '') => {
    dispatch(setLoadingOn());
    dispatch(fetchRecommendData());
    dispatch(setLoadingOff());
  };

  useEffect(() => {
    callCategory();
    callGetRecommendArticle();
    callApiGetArticle();
  }, []);
  return (
    <AllArticleContainerStyle>
      <a target="_blank" href="https://lin.ee/wtEcDjy">
        <Image className="chat-icon" src="/images/icon/chat.png" />
      </a>
      <Container maxWidth="lg" className="detail-container">
        <div className="display-flex flex-center justify-center">
          <Image component="div" src="/images/logo/8.png" fluid className="header-icon" />
          <Typography variant="h4" fontWeight={600}>
            บทความทั้งหมด
          </Typography>
        </div>
        <div className="rounded"></div>

        <Tabs
          className="category"
          value={activeTab}
          variant="scrollable"
          onChange={(e, v) => {
            handleClick(v);
            callApiGetArticle(1, constants.SHOW_ARTICLE_NUM, v);
          }}
          aria-label="scrollable auto tabs example">
          <Tab value={0} label={'ทั้งหมด'} key={`CATEGORY-ITEM-all`} />
          {categoryList.map((item, index) => (
            <Tab className="category-item" value={item.id} label={item.name} key={`CATEGORY-ITEM-${item.id}`} />
          ))}
        </Tabs>

        <Grid container direction="row" columnSpacing={{ sm: 2, md: 5, lg: 5, xl: 5 }} alignContent="center" className="mb-5 space-container">
          {allArticleList.data.map((item) => (
            <Grid item container direction="column" xs={12} sm={6} md={6} lg={4} xl={4} key={`LIST_FUNCTION_${item.key}`}>
              <CustomCard title={item.header} src={item.src} tag={item.categoryName} articleId={item.articleId} date={item.date} timeRead={item.timeRead}></CustomCard>
            </Grid>
          ))}
        </Grid>

        <Grid container direction="row" columnSpacing={5} alignContent="center" className="space-container" style={{ display: allArticleList.data.length === 0 ? 'flex' : 'none' }}>
          {allArticleList.data.length === 0 && <ArticleNotFound />}
        </Grid>

        <Grid container direction="row" justifyContent="center" justifyItems="center" alignContent="center">
          <PaginateComponent
            activePage={page}
            totalPage={allArticleList.totalPage}
            onChange={(e, page) => {
              console.log(page);
              callApiGetArticle(page, constants.SHOW_ARTICLE_NUM, activeTab);
            }}
          />
        </Grid>
      </Container>
    </AllArticleContainerStyle>
  );
});

export default AllArticleContainer;
