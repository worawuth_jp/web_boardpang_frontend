import React from 'react';
import { Image } from 'react-bootstrap';
import CustomCardStyle from './style';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import TimerIcon from '@mui/icons-material/Timer';
import color from '../../../../styles/variables/color';
import { useRouter } from 'next/dist/client/router';
import DialogLoading from '../../../shared/loadingDialog';
import { useSelector } from 'react-redux';
import { Grid } from '@mui/material';

function CustomCard({ children, title, tag, date, timeRead, src = '', articleId = 'invalid' }) {
  let router = useRouter();
  const isLoading = useSelector((state) => state.index.options.isLoading);
  return (
    <CustomCardStyle
      onClick={() => {
        router.push(`/article/${articleId}`);
      }}>
      {/* <DialogLoading open={isLoading} /> */}
      <div>
        <Image src={src} />
      </div>
      <div>
        <div>
          <div className="text-category">
            <LocalOfferIcon style={{ fontSize: 12, color: color.GOLD_COLOR_1 }} /> {tag}
          </div>
          <Title>{title}</Title>
          <div className="content-card">{children}</div>
          <div className="footer">
            <Grid container direction="row" style={{ paddingLeft: 25, paddingRight: 25 }}>
              <Grid container direction="column" item md={6} sm={12} xs={12} lg={6} alignItems="center">
                <Grid container direction="row" alignItems="center">
                  <CalendarTodayIcon className="display-flex" style={{ fontSize: 18, alignItems: 'center' }} />
                  <span className="display-flex" style={{ fontSize: 15, marginLeft: 5, alignItems: 'center' }}>
                    {date}
                  </span>
                </Grid>
              </Grid>

              <Grid container direction="column" item md={6} sm={12} xs={12} lg={6} justifyContent="end">
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  style={{ marginTop: { xs: 2, sm: 2, md: 2, lg: 2, xl: 2 } }}
                  justifyContent={{ xs: 'start', sm: 'start', md: 'end', lg: 'end', xl: 'end' }}
                  justifyItems="center">
                  <TimerIcon style={{ fontSize: 18, alignContent: 'end', alignItems: 'center' }} />
                  <span style={{ fontSize: 15, marginLeft: 5, alignItems: 'center' }}>{timeRead}</span>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </CustomCardStyle>
  );
}

function Title({ children }) {
  return <div className="title">{children}</div>;
}

export default CustomCard;
