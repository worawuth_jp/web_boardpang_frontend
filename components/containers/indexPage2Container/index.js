import { Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import IndexPage2ContainerStyle from './style';
import * as constants from '../../../constants/indexPage2Contants';
import { Image } from 'react-bootstrap';
import useWindowSize from '../../../modules/windowSize';
import windowSize from '../../../styles/variables/windowSize';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';

const IndexPage2Container = React.forwardRef((props, ref) => {
    let size = useWindowSize();
    const configYoutube = useSelector((state) => state.index.constants.configYoutube);
    const router = useRouter();
    return (
        <IndexPage2ContainerStyle className="bg-1" navHeight={props.navHeight} screenWidth={size.width} screenHeight={size.height}>
            <Image className="vector" src="/images/icon/Vector1.png" />
            <a target="_blank" href="https://lin.ee/wtEcDjy">
                <Image className="chat-icon" src="images/icon/chat.png" />
            </a>

            <div className="app-container">
                <div className="index-wrapper">
                    <Grid container direction="row" spacing="2" justifyContent="center" justifyItems="center">
                        <Grid item container xs={12} sm={12} md={6} lg={6} xl={6}>
                            <div className="header">
                                <div className="text-header-1">
                                    <br />
                                    {constants.HEADER_TEXT_1}
                                    <Image src="/images/logo/15.png" fluid className="header-icon" />
                                </div>
                                <Typography variant="h3">{constants.HEADER_TEXT_2}</Typography>
                                <div className="text-content" style={{ fontStyle: 'italic' }}>
                                    เพราะระบบบรอดแคสต์ ถือว่าเป็นเครื่องมือที่จะช่วยส่งข้อความไปถึงลูกค้าเก่า
                                    <br />
                                    ได้แบบง่ายๆ ประหยัดทั้งเงินและเวลา ซึ่งลูกค้าเก่าคือคนที่สนใจสินค้าหรือบริการ
                                    <br />
                                    ของเราอยู่แล้ว ยิ่งเรามีโปรโมชั่นเด็ดๆส่งกลับหาลูกค้าเก่าก็สามารถเพิ่มยอดขายได้
                                    <br />
                                    อย่างง่ายดายและยิ่งเรามีการสื่อสารกับลูกค้าอยู่เป็นประจำก็ทำให้ลูกค้าไม่ลืมเพจเรา
                                </div>
                            </div>

                            <div className="header display-rs">
                                <iframe
                                    width="100%"
                                    height="100%"
                                    className="video-yt"
                                    src={configYoutube.block2}
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                    style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
                            </div>

                            <div className="header text-center">
                                <Link href="/about">
                                    <Button
                                        variant="contained"
                                        size="small"
                                        className="btn-button"
                                        style={{ marginTop: '2.8%', marginBottom: '10%' }}
                                        onClick={() => {
                                            router.replace('/about');
                                        }}>
                                        เกี่ยวกับเรา
                                    </Button>
                                </Link>
                            </div>
                        </Grid>

                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={6}
                            lg={6}
                            xl={6}
                            className="display"
                            style={{
                                height: '100%',
                                marginBottom: 'auto',
                                marginTop: 'auto',
                                paddingLeft: 25,
                                paddingRight: 25,
                            }}>
                            <iframe
                                width="100%"
                                height="100%"
                                className="video-yt"
                                src="https://www.youtube.com/embed/8IB9xbP_keI"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        spacing="2"
                        justifyContent="center"
                        justifyItems="center"
                        position="absolute"
                        style={{ bottom: '3%', width: size.width >= windowSize.STD_WIDTH ? windowSize.STD_WIDTH : size.width }}>
                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={3}
                            lg={3}
                            xl={3}
                            className="display"
                            style={{
                                height: '100%',
                                marginBottom: 'auto',
                                marginTop: 'auto',
                                paddingLeft: 25,
                                paddingRight: 25,
                            }}>
                            <div className="index-footer">
                                <div className="footer-item">
                                    <Image src="/images/logo/14.png" />
                                    <div className="footer-content-1">ยอดขายเพิ่มขึ้น 300%</div>
                                </div>
                            </div>
                        </Grid>
                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={3}
                            lg={3}
                            xl={3}
                            className="display"
                            style={{
                                height: '100%',
                                marginBottom: 'auto',
                                marginTop: 'auto',
                                paddingLeft: 25,
                                paddingRight: 25,
                            }}>
                            <div className="index-footer">
                                <div className="footer-item">
                                    <Image src="/images/logo/12.png" />
                                    <div className="footer-content-2">ผู้ใช้งานมากกว่า 40000 เพจ</div>
                                </div>
                            </div>
                        </Grid>
                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={4}
                            lg={4}
                            xl={4}
                            className="display"
                            style={{
                                height: '100%',
                                marginBottom: 'auto',
                                marginTop: 'auto',
                                paddingLeft: 25,
                                paddingRight: 25,
                            }}>
                            <div className="index-footer">
                                <div className="footer-item">
                                    <Image src="/images/logo/11.png" />
                                    <div className="footer-content-3">ดูแลหลังการขายตลอดอายุดการใช้งาน</div>
                                </div>
                            </div>
                        </Grid>

                        <Grid
                            item
                            container
                            xs={12}
                            sm={12}
                            md={1}
                            lg={1}
                            xl={1}
                            className="display"
                            style={{
                                height: '100%',
                                marginBottom: 'auto',
                                marginTop: 'auto',
                                paddingLeft: 25,
                                paddingRight: 25,
                            }}></Grid>
                    </Grid>
                </div>
            </div>
            {/* <Container maxWidth="lg" className="content-container">
        <Grid container direction="row" spacing="2" justifyContent="center" justifyItems="center">
          <Grid item container xs={12} sm={12} md={6} lg={6} xl={6}>
            <div className="header">
              <Typography variant="h5">
                {constants.HEADER_TEXT_1}
                <Image src="/images/logo/15.png" fluid className="header-icon" />
              </Typography>
              <Typography variant="h4">{constants.HEADER_TEXT_2}</Typography>
              <Typography variant="p" style={{ whiteSpace: 'normal', fontStyle: 'italic' }}>
                {constants.HEADER_TEXT_3}
              </Typography>
            </div>

            <div className="header display-rs">
              <iframe
                width="100%"
                height="100%"
                className="video-yt"
                src="https://www.youtube.com/embed/8IB9xbP_keI"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
            </div>

            <div className="header text-center">
              <Button variant="contained" size="small" className="btn-button" style={{ marginTop: 10 }} onClick={() => props.handleClick()}>
                เกี่ยวกับเรา
              </Button>
            </div>
          </Grid>

          <Grid
            item
            container
            xs={12}
            sm={12}
            md={6}
            lg={6}
            xl={6}
            className="display"
            style={{
              height: '100%',
              marginBottom: 'auto',
              marginTop: 'auto',
              paddingLeft: 25,
              paddingRight: 25,
            }}>
            <iframe
              width="100%"
              height="100%"
              className="video-yt"
              src="https://www.youtube.com/embed/8IB9xbP_keI"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              style={{ marginLeft: 'auto', marginRight: 'auto' }}></iframe>
          </Grid>
        </Grid>
      </Container> */}
        </IndexPage2ContainerStyle>
    );
});

export default IndexPage2Container;
