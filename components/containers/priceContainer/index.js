import { Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { Image } from 'react-bootstrap';
import PriceContainerStyle from './style';
import * as constants from '../../../constants/priceConstants';
import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';
import { useState } from 'react';
import { useEffect } from 'react';
import { useRef } from 'react';

const PriceContainer = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState({ 1: true, 2: false, 3: false });
  const [headerHeight, setHeaderHeight] = useState(0);
  const [contentHeight, setContentHeight] = useState(0);
  const [cardHeight, setCardHeight] = useState(0);
  let router = useRouter();
  const headerRef = useRef({});
  const contentRef = useRef({});
  const cardRef = useRef({});

  const handleClick = (index) => {
    let openTemp = open;
    openTemp[index] = !openTemp[index];
    setOpen((prev) => ({ ...prev, ...openTemp }));
  };

  useEffect(() => {
    setHeaderHeight(headerRef.current.offsetHeight);
    setContentHeight(contentRef.current.offsetHeight);
    console.log(contentRef);
  }, [headerRef, contentRef, headerRef.current, contentRef.current, headerHeight, contentHeight]);
  return (
    <PriceContainerStyle className="bg" ref={ref} navHeight={props.navHeight} cardHeight={cardHeight} headerHeight={headerHeight} contentHeight={contentHeight}>
      <a target="_blank" href="https://lin.ee/wtEcDjy">
        <Image className="chat-icon" src="images/icon/chat.png" />
      </a>
      <div className="app-container">
        <div className="price-wrapper">
          <Grid ref={headerRef} container direction="row" spacing="2" textAlign="center" display="block" className="header-text-page">
            <Typography variant="div" display="flex" justifyItems="center" justifyContent="center" alignItems="center">
              <Image src="/images/logo/11.png" fluid className="header-icon" />
              <div style={{ fontSize: 42, fontWeight: 600 }}>
                {constants.HEADER_PAGE_TEXT}
                <div className="text-description">{constants.HEADER_TEXT_1}</div>
              </div>
            </Typography>
          </Grid>
          <Container className="price-card-zone" maxWidth="xxl">
            <Grid container direction="row" columnSpacing={{ sm: 2, md: 4 }} display="flex" textAlign="center" alignContent="center" alignItems="start">
              {/* {constants.LIST_DATA_PRICE.map((item) => (
              <Grid container flex={1} item xs={12} sm={4} md={4} lg={4} xl={4} key={item.key}>
                <div className="cards">
                  <div className="card-header">
                    <div className="text-header">
                      <span>{item.header}</span>
                      {item.bestSell && <div className="best-sell">ขายดี</div>}
                    </div>
                  </div>
                  <div className="card-detail">
                    <div className="price">{item.currentPrice} บาท</div>
                    <span className="">
                      (ปกติ <span className="old-price">{item.oldPrice}</span> บาท)
                    </span>

                    {item.listDescription.map((description) => (
                      <div className="list-text text-muted" key={`Description_${description.key}`}>
                        <Typography variant="p" className="list-item">
                          <Image src="images/icon/check-mark (1).png" />
                          {description.content}
                        </Typography>
                      </div>
                    ))}
                  </div>

                  <div className="card-footer">
                    <Link href={constants.LINE_LINK}>
                      <a target="_blank">
                        <Button variant="contained" size="small" className="btn-button">
                          เลือกแพ็คเกจนี้
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </Grid>
            ))} */}

              <Grid container item xs={12} sm={4} md={4} lg={4} xl={4} key={constants.LIST_DATA_PRICE[0].key}>
                <div className="card-add">
                  <div className="card-header">
                    <div className="text-header">
                      <span>{constants.LIST_DATA_PRICE[0].header}</span>
                      {constants.LIST_DATA_PRICE[0].bestSell && <div className="best-sell">ขายดี</div>}
                    </div>
                  </div>
                  <div className="card-detail">
                    <div className="price">{constants.LIST_DATA_PRICE[0].currentPrice} บาท</div>
                    <span className="">
                      (ปกติ <span className="old-price">{constants.LIST_DATA_PRICE[0].oldPrice}</span> บาท)
                    </span>

                    {constants.LIST_DATA_PRICE[0].listDescription.map((description) => (
                      <div className="list-text text-muted" key={`Description_${description.key}`}>
                        <Typography variant="p" className="list-item">
                          <Image src="images/icon/check-mark (1).png" />
                          {description.content}
                        </Typography>
                      </div>
                    ))}
                  </div>

                  <div className="card-footer">
                    <Link href={constants.LINE_LINK}>
                      <a target="_blank">
                        <Button variant="contained" size="small" className="btn-button">
                          เลือกแพ็คเกจนี้
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </Grid>

              <Grid container item xs={12} sm={4} md={4} lg={4} xl={4} key={constants.LIST_DATA_PRICE[1].key}>
                <div className="card-add">
                  <div className="card-header">
                    <div className="text-header">
                      <span>{constants.LIST_DATA_PRICE[1].header}</span>
                      {constants.LIST_DATA_PRICE[1].bestSell && <div className="best-sell">ขายดี</div>}
                    </div>
                  </div>
                  <div className="card-detail">
                    <div className="price">{constants.LIST_DATA_PRICE[1].currentPrice} บาท</div>
                    <span className="">
                      (ปกติ <span className="old-price">{constants.LIST_DATA_PRICE[1].oldPrice}</span> บาท)
                    </span>

                    {constants.LIST_DATA_PRICE[1].listDescription.map((description) => (
                      <div className="list-text text-muted" key={`Description_${description.key}`}>
                        <Typography variant="p" className="list-item">
                          <Image src="images/icon/check-mark (1).png" />
                          {description.content}
                        </Typography>
                      </div>
                    ))}
                  </div>

                  <div className="card-footer">
                    <Link href={constants.LINE_LINK}>
                      <a target="_blank">
                        <Button variant="contained" size="small" className="btn-button">
                          เลือกแพ็คเกจนี้
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </Grid>

              <Grid container item xs={12} sm={4} md={4} lg={4} xl={4} key={constants.LIST_DATA_PRICE[2].key}>
                <div className="cards">
                  <div className="card-header">
                    <div className="text-header">
                      <span>{constants.LIST_DATA_PRICE[2].header}</span>
                      {constants.LIST_DATA_PRICE[2].bestSell && <div className="best-sell">สุดคุ้ม</div>}
                    </div>
                  </div>
                  <div ref={contentRef} className="card-detail">
                    <div className="price">{constants.LIST_DATA_PRICE[2].currentPrice} บาท</div>
                    <span className="">
                      (ปกติ <span className="old-price">{constants.LIST_DATA_PRICE[2].oldPrice}</span> บาท)
                    </span>

                    {constants.LIST_DATA_PRICE[2].listDescription.map((description) => (
                      <div className="list-text text-muted" key={`Description_${description.key}`}>
                        <Typography variant="p" className="list-item">
                          <Image src="images/icon/check-mark (1).png" />
                          {description.content}
                        </Typography>
                      </div>
                    ))}
                  </div>

                  <div className="card-footer">
                    <Link href={constants.LINE_LINK}>
                      <a target="_blank">
                        <Button variant="contained" size="small" className="btn-button">
                          เลือกแพ็คเกจนี้
                        </Button>
                      </a>
                    </Link>
                  </div>
                </div>
              </Grid>
            </Grid>
            {/* <div className="div-space"></div> */}
          </Container>
        </div>
      </div>
    </PriceContainerStyle>
  );
});

export default PriceContainer;
