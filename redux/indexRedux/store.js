const state = {
    options: {
        isLoading: false,
    },
    constants: {
        articleList: [],
        recommendArticle: [],
        articleInfo: {},
        randomArticle: [],
        category: [],
        allArticleList: {
            activePage: 1,
            totalPage: 1,
            totalRecord: 0,
            data: [],
        },
        configYoutube: {
            block1: '',
            block2: '',
            block3: '',
        },

        userReview: [],
        youtubeReview: [],
    },
};
export default state;
