export const SET_LOADING_ON = 'SET_LOADING_ON';
export const SET_LOADING_OFF = 'SET_LOADING_OFF';
export const CALL_API_FAIL = 'CALL_API_FAIL';
export const CALL_API_SUCCESS = 'CALL_API_SUCCESS';
export const CLOSE_ALERT = 'CLOSE_ALERT';
export const SET_NAV_KEY = 'SET_NAV_KEY';
export const CLEAR_NAV_KEY = 'CLEAR_NAV_KEY';
