const state = {
  options: {
    isLoading: false,
    alert: {
      open: false,
      success: false,
      message: '',
      code: '',
    },
  },
  constants: {
    navKey: '',
  },
};
export default state;
