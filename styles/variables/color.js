export default {
  PRIMARY: '#FCED20',
  SECONDARY: '#000000',
  BLACK_COLOR: '#000000',
  WHITE_COLOR: '#FFFFFF',

  YELLOW_COLOR_1: '#FCED20',
  YELLOW_COLOR_2: '#F4EA62',
  YELLOW_COLOR_3: '#FFFEF7',

  GRAY_COLOR_1: '#B0B0B0',
  GRAY_COLOR_2: '#625E5D',
  GRAY_COLOR_3: '#51514f',
  GRAY_COLOR_4: '#51514f1f',

  RED_COLOR_1: '#ed1b24',

  BLUE_COLOR_1: '#0074FB',

  GOLD_COLOR_1: '#F3C54C'
};
